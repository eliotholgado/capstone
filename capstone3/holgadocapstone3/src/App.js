import './App.css';
import AppNavBar from './components/AppNavBar.js'
import Register from './pages/Register.js'
import Login from './pages/Login.js'
import Products from './pages/Products.js'
import Logout from './pages/Logout.js'
import NotFound from './pages/NotFound.js'
import AdminDashboard from './pages/AdminDashboard.js'
import CreateProduct from './pages/CreateProduct.js'
import AdminProduct from './pages/AdminProduct.js'
import UserDashboard from './pages/UserDashboard.js'
import ProductView from './pages/ProductView.js'
import AvailableProducts from './pages/AvailableProducts.js'

import {useState, useEffect} from 'react'
import {BrowserRouter, Route, Routes} from 'react-router-dom'
import {UserProvider} from './UserContext.js'
import 'bootstrap/dist/css/bootstrap.min.css'


function App() {
  
  const [user, setUser] = useState ({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(() => {
    if (!localStorage.getItem('token')) {
      unsetUser();
    }
  }, []);

  useEffect(()=>{
  if(localStorage.getItem('token')){
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
      setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
    })  
  }
},[])

  return (
    <div className="App">

      <UserProvider value = {{user, setUser, unsetUser}}>
        <BrowserRouter>
          <AppNavBar/>

            <Routes>
              <Route path = '/' element = {<AvailableProducts/>} />
              <Route path = '/products' element = {<Products/>}/>
              <Route path = '/register' element = {<Register/>}/>
              <Route path = '/login' element = {<Login/>}/>
              <Route path = '/logout' element = {<Logout/>}/>
              <Route path = '/adminDashboard' element = {<AdminDashboard/>} />
              <Route path = '/createProduct' element = {<CreateProduct/>} />
              <Route path = '/adminProduct' element = {<AdminProduct/>} />
              <Route path = '/userDashboard' element = {<UserDashboard/>} />
              <Route path = '/products/:productId'element = {<ProductView/>} />
              <Route path = '*' element = {<NotFound/>}/>
            </Routes>

        </BrowserRouter>
      </UserProvider>
    </div>
  );
}

export default App;
