import {Container, Nav, Navbar, Accordion} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'
import {useState, useContext} from 'react'
import UserContext from '../UserContext.js'

export default function AppNavBar () {

	const {user, setUser} = useContext(UserContext)
	return (
		<>

		<Navbar className = 'navbar d-none d-md-flex'>
			<Container>
				<Navbar.Brand as = {Link} to = '/'>E-Shop</Navbar.Brand>
				<Nav className="ms-auto">
					
					{
						(user.isAdmin !== true) ?
						<>
							<Nav.Link  as = {Link} to = '/' className = 'navlink'>Products</Nav.Link>
						</>
						:
						<>
							<Nav.Link  as = {Link} to = '/adminDashboard' className = 'navlink'>Admin Dashboard</Nav.Link>
							<Nav.Link  as = {Link} to = '/' className = 'navlink'>Products</Nav.Link>
						</>

					}

					{
						(user.id === null && user.isAdmin !== true) ?
						<>
							<Nav.Link  as = {Link} to = '/register' className = 'navlink'>Register</Nav.Link>
							<Nav.Link  as = {Link} to = '/login' className = 'navlink'>Login</Nav.Link>
						</>
						:
						<Nav.Link  as = {Link} to = '/logout' className = 'navlink'>Logout</Nav.Link>
					}
					
					
				</Nav>
			</Container>
		</Navbar>
		</>
	)
}