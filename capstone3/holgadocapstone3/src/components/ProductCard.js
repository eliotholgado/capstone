import {Container, Row, Col, Button, Card} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Link, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext.js'
import ProductProp from '../pages/Products.js'
import Swal2 from 'sweetalert2'

export default function ProductCard(props) {

	const {_id, name, description, isAvailable, createdOn, price, stock} = props.ProductProp
	const {user, setUser} = useContext(UserContext)

	return(
			<Col className = 'col-md-4 col-sm-12 mb-3 mx-auto text-center'>
				<Card>
					<Card.Body>
						<img className = 'shoes' src={process.env.PUBLIC_URL + `/images/${_id}.jpg`} alt='No image provided'/>

						<Card.Title>{name}</Card.Title>
							
						<Card.Subtitle className = 'mt-3'>Description:</Card.Subtitle>
							
						<Card.Text>
						{description}
						</Card.Text>

						<Card.Subtitle className = 'mt-3 mb-2'>Price: Php {price}</Card.Subtitle>

						<Card.Subtitle className = 'mt-3 mb-2'>In Stock: {JSON.stringify(stock)}</Card.Subtitle>

						<Card.Subtitle className='mt-3 mb-2'>
						  {isAvailable ? "Available" : "Unavailable"}
						</Card.Subtitle>

					</Card.Body>
				</Card>
			</Col>
	)
}