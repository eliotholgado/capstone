import {Container, Row, Button} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom'
import AdminProduct from './AdminProduct.js'

export default function adminDashboard () {

	const navigate = useNavigate()

	const goToCreateProduct = () => {
		navigate('/createProduct')
	}

	const goToProducts = () => {
		navigate('/products')
	}

	return (
		<>
			<h1 className='text-center mt-3'>Admin Dashboard</h1>
			<Container className='text-center mt-3'>
				<Row className='justify-content-center'>
					<Button id='goToCreateProductButton' className = 'me-2' onClick={goToCreateProduct}>
					Create Product
					</Button>

					<Button id='goToProductsButton' onClick={goToProducts}>
					See Products
					</Button>
				</Row>
			</Container>
			<AdminProduct/>
		</>
	)
}