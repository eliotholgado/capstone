import UserProductCard from '../components/UserProductCard.js'
import {useEffect, useState} from 'react'
import {Container, Row} from 'react-bootstrap'

export default function UserDashboard () {
    
    const [products, setProducts] = useState([])
    
    useEffect(()=>{
        fetch(`${process.env.REACT_APP_API_URL}/products/retrieveAvailableProducts`)
        .then(result => result.json())
        .then(data => {
            setProducts(data)
        })
    }, [])

    return(
        <>
            <h1 className = 'm-3 text-center'>Products Available</h1>
            <Container>
                <Row>
                    <>
                        {products.map(product => (
                            <UserProductCard key={product._id} UserProductProp={product} />
                        ))}
                    </>
                </Row>
            </Container>
        </>
    )
}