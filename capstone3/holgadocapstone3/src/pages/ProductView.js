import {useState, useEffect, useContext} from 'react'
import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import Swal2 from 'sweetalert2'
import {useNavigate, useParams} from 'react-router-dom'
import UserContext from '../UserContext.js'

export default function ProductView () {
	
	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [stock, setStock] = useState('')
	const [totalItems, setTotalItems] = useState(0)
	const [totalPrice, setTotalPrice] = useState(0)
	const [isPlusDisabled, setIsPlusDisabled] = useState(false)
	const [isMinusDisabled, setIsMinusDisabled] = useState(false)

	const {productId} = useParams()
	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(result => result.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setStock(data.stock)
		})
	}, [])

	useEffect(()=>{
		if (stock === 0){
			Swal2.fire({
				title: 'Out of Stock!',
				icon: 'info',
				text: 'Item is no longer available'
			})
			setIsPlusDisabled(true)
		}else{
			setIsPlusDisabled(false)
		}
	}, [stock])

	useEffect(()=>{
		if (totalItems === 0){
			setIsMinusDisabled(true)
		}else{
			setIsMinusDisabled(false)
		}
	}, [totalItems])

	const add = (event) => {
		event.preventDefault()
		setTotalItems(totalItems+1)
		setTotalPrice(price*(totalItems+1))
		setStock(stock-1)
	}

	const subtract = (event) => {
		event.preventDefault()
		setTotalItems(totalItems-1)
		setTotalPrice(price*(totalItems-1))
		setStock(stock+1)
	}

	const checkout = (event) => {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/orderProduct`, {
			method: 'POST',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				id: productId
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data===true){
				Swal2.fire({
					title: 'Checkout Successful',
					icon: 'success',
					text: 'Thank you for ordering!'
				})
			}else{
				Swal2.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})

		fetch(`${process.env.REACT_APP_API_URL}/products/updateStock`, {
			method: 'PATCH',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				productId: productId,
				stock: stock
			})
		})
		.then(result => result.json())
		.then(data => console.log(data))

		navigate('/')
	}

	return (
		<>
		<Container className = 'col-6 mt-3'>
			<Row>
				<Col>
					<Card style={{ width: '100%' }} className = 'text-center mx-auto'>

						<Card.Body>
							<Card.Title className = 'mb-3'>{name}</Card.Title>
							
							<img className = 'shoesView' src={process.env.PUBLIC_URL + `/images/${productId}.jpg`} alt='No image provided'/>

							<Card.Subtitle className='mt-3'>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>{price}</Card.Text>

							<Card.Subtitle>Stock</Card.Subtitle>
							<Card.Text>{stock}</Card.Text>

							<div className='m-2'>
								<Button id = 'addButton' onClick = {() => add(event)} disabled = {isPlusDisabled}>+</Button>

								<Button id = 'subtractButton' className="ms-2" onClick = {() => subtract(event)} disabled = {isMinusDisabled}>-</Button>

								<div>
									<Card.Title className='mt-3'>Total Items: {totalItems}</Card.Title>

									<Card.Title>Total Price: Php {totalPrice}</Card.Title>
								</div>
							</div>

							<Button id = 'checkoutButton' variant="primary" onClick = {event => checkout(event)}>Checkout</Button>

						</Card.Body>

					</Card>
				</Col>
			</Row>
		</Container>
		</>
	)
}