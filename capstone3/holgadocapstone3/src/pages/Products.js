import ProductCard from '../components/ProductCard.js'
import {useEffect, useState} from 'react'
import {Container, Row} from 'react-bootstrap'

export default function products () {
	
	const [products, setProducts] = useState([])

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/products/retrieveAllProducts`)
		.then(result => result.json())
		.then(data => {
			setProducts(data)
		})
	},[])

	return (
		<>
		<h1 className = 'm-3 text-center'>Products</h1>
		<Container>
			<Row>
				<>
					{products.map(product => (
						<ProductCard key={product._id} ProductProp={product} />
					))}
				</>
			</Row>
		</Container>
		</>
	)

}