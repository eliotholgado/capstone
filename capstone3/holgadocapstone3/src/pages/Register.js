import {Container, Row, Col, Button, Form} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Link, Navigate, useNavigate, useLocation} from 'react-router-dom'
import Swal2 from 'sweetalert2'
import UserContext from '../UserContext'

export default function Register () {

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [isDisabled, setIsDisabled] = useState(true)

	const {user, setUser} = useContext(UserContext)

	const navigate = useNavigate()
	const location = useLocation()

	useEffect(()=>{

		if(firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && mobileNo !== '' && password1 === password2 && password1.length !== 0) {
			setIsDisabled(false)
		}

	}, [firstName, lastName, email, password1, password2, mobileNo])

	useEffect(()=>{
		if(location.pathname === '/register' && localStorage.getItem('token')){
			navigate('*')
		}
	}, [location.pathname, navigate])

	function registerNow (event) {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/registerUser`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				mobileNo: mobileNo
			})
		})
		.then(result => result.json())
		.then(data => {
			console.log(data)
			if(data){
				/*localStorage.setItem('email', email)
				setUser(localStorage.getItem('email'))*/

				Swal2.fire({
					title: 'Registration Successful',
					icon: 'success',
					text: 'Thank you for registering!'
				})				
				navigate('/login')
			}else{
				Swal2.fire({
					title: 'Email is already taken',
					icon: 'error',
					text: 'Please choose a different email'
				})
			}
		})
	}

	return(
		<Container>
			<Row>
				<Col className = 'mx-auto col-6 m-3'>
					<h1 className='text-center'> Register Now </h1>
					<Form onSubmit = {event => registerNow(event)}>

						{/*First Name*/}
						<Form.Group className="mb-3" controlId="formFirstName">
							<Form.Label>First Name</Form.Label>
							<Form.Control 
								type="firstName" 
								placeholder="First Name" 
								value={firstName}
								onChange = {event => setFirstName(event.target.value)}
							/>
						</Form.Group>

						{/*Last Name*/}
						<Form.Group className="mb-3" controlId="formLastName">
							<Form.Label>Last Name</Form.Label>
							<Form.Control 
								type="lastName" 
								placeholder="Last Name" 
								value={lastName}
								onChange = {event => setLastName(event.target.value)}
							/>
						</Form.Group>

						{/*Mobile Number*/}
						<Form.Group className="mb-3" controlId="formMobileNo">
							<Form.Label>Mobile Number</Form.Label>
							<Form.Control 
								type="number" 
								placeholder="Please enter a valid 11-digit mobile number" 
								value={mobileNo}
								onChange = {event => setMobileNo(event.target.value)}
							/>
						</Form.Group>

						{/*Email*/}
						<Form.Group className="mb-3" controlId="formBasicEmail">
							<Form.Label>Email address</Form.Label>
							<Form.Control 
								type="email" 
								placeholder="Enter email" 
								value={email}
								onChange = {event => setEmail(event.target.value)}
							/>
						</Form.Group>

						{/*Password1*/}
						<Form.Group className="mb-3" controlId="formBasicPassword1">
							<Form.Label>Password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Password should be at least 6 characters"
								value={password1}
								onChange = {event => setPassword1(event.target.value)}
							/>
						</Form.Group>

						{/*Password1*/}
						<Form.Group className="mb-3" controlId="formBasicPassword2">
							<Form.Label>Confirm Password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Re-Enter your password"
								value={password2}
								onChange = {event => setPassword2(event.target.value)}
							/>
						</Form.Group>

						<p>Have an account already? Login <Link to = '/login' style = {{textDecoration:'none'}}>here</Link></p>

						{/*Submit*/}
						<div className="d-grid">
							<Button id = "registerButton" type="submit" className="text-center mx-auto" disabled = {isDisabled}>
								Submit
							</Button>
						</div> 

					</Form>
				</Col>
			</Row>
		</Container>

	)
}